#include<stdio.h>
#include<sys/stat.h>


int main(int argc, char **argv)
{
	struct stat mi_stat;
	
	//TODO: Verificar el uso de la opci�n -f

	if (argc == 2){

		/* Ejemplo como verificar existencia y tama�o de un archivo */
		if(stat(argv[1],&mi_stat) < 0)
			fprintf(stderr, "Archivo %s no existe!\n", argv[1]);
		else
			printf("El archivo %s tiene %ld bytes!\n", argv[1],mi_stat.st_size);

		//TODO: Leer archivo usando rio_readn
		//TODO: Calcular SHA1
		//TODO: Mostrar SHA1 en consola en formato hexadecimal
		//TODO: Escribir SHA1 en archivo <nombre_archivo>.sha1 usando rio_writen o fprintf
		
	}else if (argc == 3){
		//TODO: Leer archivo usando rio_readn
		//TODO: Calcular SHA1
		//TODO: Leer archivo con SHA1 usando rio_readn (si se us� -f)
		//TODO: Comparar ambos SHA1
		//TODO: Mostrar resultado comparaci�n
	}else
		fprintf(stderr, "uso: %s <nombre_archivo> [(-f <nombre_archivo_sha1> | <hash_sha1>)]\n", argv[0]);
}
